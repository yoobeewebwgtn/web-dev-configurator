# WEB DEVELOPMENT SHELL CONFIGURATOR #

Setting up a Yoobee lab computer for doing web development manually is time consuming and full of many steps. This script does all the magic for you, reliably.

## What it does
* Configures the environment variables `$http_proxy` and `$https_proxy` with your username and password, so command line tools such as `git` and `composer` can download things.
* Improves the command line interface by enabling color in both `bash` and `git`.
* Sets up `git` with user's name and email address and good defaults.
* Sets the command line editor to `nano` because it's easier to use than `vim` (Mac only).
* Adds lots of bash aliases to make it easier for people used to Windows or MS-DOS.
* Sets up XAMPP's `php` as the main `php` to use from the command line, as Mac's one is often misconfigured.
* Installs `composer` and `laravel` command line tools.
* Configures `npm` to use the user's home folder when installing things globally
* Installs `gulp` command line tool.
* Writes to the user's `~/.bash_profile` to add aliases and set environment variables

## Installing the Configurator

This will automatically download and create `webdev.sh` in your user home directory on your computer:

1. Copy the following to the clipboard: (triple click the grey box, then Command+C on a Mac, or Control+C on Windows) 

	```
	curl -sSL https://bitbucket.org/yoobeewebwgtn/web-dev-configurator/raw/master/webdev.sh > ~/webdev.sh ; chmod u+x ~/webdev.sh
	```

2. Open the Terminal application on a Mac, or Git Bash on Windows

3. Paste it into prompt and press enter: Command+V on Mac, Insert key on Windows.

## Using the Configurator

1. To run the script, type into the Terminal / Git Bash window:
    
    ```
    ~/webdev.sh
    ```

Read and follow instructions on the screen. Watch for potential errors. 

* You may be asked for your Yoobee login username and password to get through the proxy. When asked for your password, for your protection, no asterisks or characters will be shown as you type.
* You will be asked for your Real Name and your Github Email address. These are used by Git to use to mark who made your commits.
* You may be asked for your GitHub credentials (username and password), have them ready.

### Student's Personal Laptops

If you are running this script on your own student laptop, the script will set up the command `yoobeeproxy`, which you will need to run before any command line tools will be able to access the internet at Yoobee.

## Shell Commands

* `attrib` -- alias to `chmod`
* `chdir` -- alias to `cd`
* `copy` -- alias to `cp`
* `d` -- alias to `dir`
* `del` -- alias to `rm`
* `deltree` -- alias to `rm -rf`
* `edit` -- alias to `$EDITOR` (defaults to `nano` on Mac and `vim` on Windows)
* `l.` -- alias to `ls -d .*`
* `ll` -- alias to `ls -la`
* `mem` -- alias to `top`
* `move` -- alias to `mv`
* `search` -- alias to `grep`

### Editors

* `vi` -- alias to `vim`
* `pico` - alias to `nano`
* `nano` -- alias to `nano -w`
* `subl` -- starts Sublime Text from the command line. `subl .` will open the present working folder in Sublime Edit.

### Mac Finder

* `showfiles` -- restarts Finder, revealing hidden files and folders
* `hidefiles` -- restarts Finder, concealing hidden files and folders

### PHP

* `phpserve` -- alias to `php -S localhost:7999`, serves the current working folder at http://localhost:7999
* `composer` -- install and manage PHP libraries and frameworks
* `laravel` -- create new Laravel projects

### Node.js

* `npm` -- configured to install global things in ~/bin
* `gulp` -- run a project's Gulp tasks

## Git Configuration

This script will run these `git config` commands:

* `git config --global push.default simple`
* `git config --global branch.autosetuprebase always`
* `git config --global core.autocrlf input`
* `git config --global color.ui true`

as well as setting up http proxy settings with necessary values.

* `git config --global http.proxy http://un:pw@proxy:port`
* `git config --global https.proxy http://un:pw@proxy:port`

### Git Aliases

* `git lg` -- one line log, git alias `git log --oneline --decorate --all --graph"
* `git s` -- git alias to `git status -s`

## Starting Over

* `./webdev.sh reset` will destroy many things, so you can your command line adventure start over: `~/.composer` `~/bin` `~/.bash_profile` `~/.npm` `~/.npmrc`

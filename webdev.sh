#!/usr/bin/env bash
#
# Get the latest version at:
# https://bitbucket.org/yoobeewebwgtn/web-dev-configurator
#

# FUNCTIONS

function reset_profile {
	rm -rf ~/.composer ~/bin ~/.bash_profile ~/.npm ~/.npmrc
	git config --global --unset http.proxy
	git config --global --unset https.proxy
	if [[ ! -z $WINDIR ]]; then
		rm -rf ~/AppData/Roaming/Composer
		rm -rf ~/AppData/Local/Composer
	fi
	echo_error "*****************************************************************"
	echo_error "*    RESET SUCCESSFUL, CLOSE ALL YOUR TERMINAL WINDOWS, NOW!    *"
	echo_error "*****************************************************************"
}

function test_for_binary {
	# which $1
	if [[ -z $(which $1) ]] ; then
		echo_error "FATAL: $1 not installed."
		echo " "
		exit
	fi
}

function init_colors {
	COLOR_RESET=$'\x1B[0m'
	COLOR_SUCCESS=$'\x1B[0;42;97m'   # 0 reset, 37 green bg,  97 high intensity white fg
	COLOR_ERROR=$'\x1B[0;41;97m'     # 0 reset, 41 red bg,    97 high intensity white fg
	COLOR_WARNING=$'\x1B[0;43;30m'   # 0 reset, 43 yellow bg, 30 black fg
	COLOR_INFO=$'\x1B[0;44;97m'      # 0 reset, 44 blue bg,   97 high intensity white fg
	COLOR_EOL=$'\x1B[K'
}

function echo_error {
	echo -en "${COLOR_ERROR}${COLOR_EOL}$1${COLOR_EOL}${COLOR_RESET}\n"
}

function echo_success {
	echo -en "${COLOR_SUCCESS}${COLOR_EOL}$1${COLOR_EOL}${COLOR_RESET}\n"
}

function echo_warning {
	echo -en "${COLOR_WARNING}${COLOR_EOL}$1${COLOR_EOL}${COLOR_RESET}\n"
}

function echo_info {
	echo -en "${COLOR_INFO}${COLOR_EOL}$1${COLOR_EOL}${COLOR_RESET}\n"
}

function init_variables {
	# used by add_to_bashprofile to note if a dated comment has been added yet
	bashprofile_uncommented=1

	profile_script=$HOME/.bash_profile

	# gitname is the configured username for git, if provided
	gitname=$(git config --global --get user.name)

	# niceuser is the logged in username, split on fullstop, and the first letters capitalised.
	niceuser=$(echo $USER | awk -F'.' '{for (i=1;i<=NF;i++){sub(".",substr(toupper($i),1,1),$i)} print}')

	# name is gitname, or if that's blank, niceuser.
	name=${gitname:-$niceuser}

	# proxy host
	proxy_host=proxy:3128

	# test_url is a page that is on the internet we can use to see if we're online
	test_url=http://www.apple.com/library/test/success.html

	#
	curl_cmd="curl -s -w %{http_code} -L -m 15 --connect-timeout 15 -o /dev/null"
	# curl 					# download a file
	# -s 					# silence progress, errors
	# -w "%{http_code}" 	# output only the http status code
	# -L 					# following redirections
	# -m 15					# don't take more than 15 seconds to do it
	# --connect-timeout 15 	# if you can't connect in 15 seconds, abort
	# -o /dev/null 			# and turf any output you generate

	test_cmd="$curl_cmd $test_url"
}

function welcome {
	echo -en "\x1B[35mY\x1B[96mO\x1B[34mO\x1B[91mB\x1B[92mE\x1B[93mE\x1B[39m" # YOOBEE
	echo " WEB DEVELOPMENT SHELL CONFIGURATOR wgtn"
	echo "---------------------------------------------------"
	echo ""
	echo "   Welcome, $name!"
	echo ""
	echo "   This shell script configures your terminal session to be able to connect"
	echo "   to the internet."
	echo "   It also tells git who you are, and configures it to use the proxy."
	echo ""
	echo_warning "   IMPORTANT: Your proxy password will be stored in PLAIN-TEXT in\n   $profile_script and $HOME/.gitconfig "
	echo " "
	echo "   We'll also set up Composer, PHP's dependancy manager, "
	echo "   and test install Laravel Installer."
	echo ""
	echo "   http://git-scm.com/   https://getcomposer.org/   http://laravel.com/"
	echo ""
	echo "-------------------------------------------------------"
	echo "   - Press Control+C to cancel this script at any time."

	read -p "   - Press [Return] to continue..." key
}

function make_sites_directory {
	if [[ -z $WINDIR ]] && [[ ! -d ~/Sites ]]; then
		mkdir ~/Sites
	fi
}

function detect_proxy {
	echo -e "-------------------------------------------------------"
	echo -en "\n-> Connecting to the internet..."

	statuscode=$($test_cmd)

	echo -en "$statuscode"

	if [[ $statuscode -eq 200 ]]; then
		echo -e " online!"
	else
		while true
		do
			echo -e " behind the proxy."
			echo -e "\n-> Command Line internet access via Yoobee's HTTP Proxy"
			read -p "   Yoobee Proxy Username: [$USER] " proxy_username
			proxy_username=${proxy_username:-$USER}
			echo "(just press enter to skip command line internet access)"
			read -s -p "   Yoobee Proxy Password: " proxy_password
			echo -e "\n";
			if [[ -z $proxy_password ]] ; then
				echo "...Skipping command line internet access."
				break
			else
				statuscode=$($test_cmd --proxy http://$proxy_username:$proxy_password@$proxy_host)

				if [[ $statuscode -eq 200 ]] ; then
					echo -e "Got online!\n"
					save_proxy

					break # while
				fi
				echo -e "-> Couldn't get online:\n $statuscode\n -> Was your password correct?"
			fi
		done
	fi
}

function save_proxy {
	# assume if internet is coming through ethernet port, this is a Yoobee computer
	if [[ -z $WINDIR ]] ; then
		onLAN=$(ifconfig en0 | grep inet)
	fi
	if [[ -z $onLAN ]] ; then
		read -p "Is this computer permanently located at Yoobee? (Y/n) " yoobee_lab
	else
		yoobee_lab=Y
	fi
	yoobee_lab=${yoobee_lab:-"y"}
	case $yoobee_lab in
		[Yy]* )
			echo "-> At Yoobee permanently."
			echo "-> Saving proxy setup and password to $profile_script"

			add_to_bashprofile "export http_proxy=http://$proxy_username:$proxy_password@$proxy_host"
			add_to_bashprofile "export https_proxy=http://$proxy_username:$proxy_password@$proxy_host"
			;;
		[Nn]* )
			echo "-> At Yoobee sometimes."
			echo "-> Saving proxy setup and password to $profile_script"

			add_to_bashprofile "alias yoobeeproxy='export http_proxy=http://$proxy_username:$proxy_password@$proxy_host; export https_proxy=http://$proxy_username:$proxy_password@$proxy_host'"
			export http_proxy=http://$proxy_username:$proxy_password@$proxy_host
			export https_proxy=http://$proxy_username:$proxy_password@$proxy_host
			;;
	esac
}

function enable_clicolor {
	if [[ -z $CLICOLOR ]] ; then
		echo -e "\n-> Enabling CLICOLOR on command line"
		add_to_bashprofile "export CLICOLOR=1 # enable colours on command line"
	fi

}

function add_helpful_aliases {
	if [[ -z $(alias -p attrib) ]]; then
		echo -e "\n-> Adding many helpful command aliases to $profile_script"
		add_to_bashprofile "alias attrib='chmod'"
		add_to_bashprofile "alias chdir='cd'"
		add_to_bashprofile "alias copy='cp'"
		add_to_bashprofile "alias d='dir'"
		add_to_bashprofile "alias del='rm'"
		add_to_bashprofile "alias deltree='rm -r'"
		add_to_bashprofile "alias edit='\$EDITOR'"
		add_to_bashprofile "alias l.='ls -d .*'"
		add_to_bashprofile "alias ll='ls -la'"
		add_to_bashprofile "alias mem='top'"
		add_to_bashprofile "alias move='mv'"
		add_to_bashprofile "alias search='grep'"
		add_to_bashprofile "alias vi='vim'"
		add_to_bashprofile "alias phpserve='php -S localhost:7999'"
		if [[ -z $WINDIR ]]; then
			add_to_bashprofile "alias pico='nano'"
			add_to_bashprofile "alias nano='nano -w'"
			add_to_bashprofile "alias showfiles='defaults write com.apple.finder AppleShowAllFiles YES; killall Finder'"
			add_to_bashprofile "alias hidefiles='defaults write com.apple.finder AppleShowAllFiles NO; killall Finder'"
		fi
		add_to_bashprofile " "
	fi
}

function make_userbin_directory {
	if [[ ! -d $HOME/bin ]]; then
		echo -e "\n-> Creating ~/bin"
		mkdir -p $HOME/bin
	fi
}

function add_userbin_to_path {
	if [[ ":$PATH:" != *":$HOME/bin:"* ]] ; then
		echo -e "\n-> Adding ~/bin to your PATH"
		add_to_bashprofile "export PATH=~/bin:\$PATH # include user's command line binaries, including composer"
	fi
}

function setup_subl_symlink {
	if [[ ! -z $(which subl) ]] ; then
		echo -en "\n-> Sublime Edit already set up on command line.\n"

	else
		# places where subl might be found
		subl_locations=(
			/Applications/Sublime\ Text.app/Contents/SharedSupport/bin/subl
			/c/Program\ Files/Sublime\ Text\ 3/subl.exe
			~/Applications/Sublime\ Text.app/Contents/SharedSupport/bin/subl
			/Applications/Sublime\ Text\ 2.app/Contents/SharedSupport/bin/subl
			/c/Program\ Files/Sublime\ Text\ 2/subl.exe
			~/Applications/Sublime\ Text\ 2.app/Contents/SharedSupport/bin/subl
		)

		for i in "${subl_locations[@]}" ; do
			if [[ -a $i ]] && [[ ! -f ~/bin/subl ]] ; then
				sublime_location=$i
				echo -e "\n-> Enabling Sublime Edit on command line."
				echo -e "\n    found subl at: "
				echo -e "  > $sublime_location"
				ln -s "$sublime_location" ~/bin/subl
				break
			fi
		done
	fi
}

function setup_cli_editor {
	if [[ -z $WINDIR ]] && [[ -z $EDITOR ]] ; then
		echo -e "\n-> Setting default EDITOR on command line to nano."
		echo " "
		add_to_bashprofile " "
		add_to_bashprofile "# comment/uncomment which CLI text editor you want Git and other tools to use."
		add_to_bashprofile " "
		add_to_bashprofile "export EDITOR='nano'     # nano: very user friendly, not very powerful"
		add_to_bashprofile "#export EDITOR='vim'     # vim: powerful, default on most systems, very different from most editors. Learn with 'vimtutor', quit with ESC, :q!"
		add_to_bashprofile "#export EDITOR='emacs'   # emacs: powerful, complicated. learn with ^h t, quit with ^x^c"
		if [[ ! -z $sublime_location ]]; then
			add_to_bashprofile "#export EDITOR='subl --wait' # Sublime Editor: Opens the editor and will wait until you save and close the file."
		fi
		add_to_bashprofile " "
		add_to_bashprofile "export VISUAL='\$EDITOR'"
		add_to_bashprofile "export PAGER=less"
		add_to_bashprofile " "

		echo_info "    Want to use vim or subl as your CLI editor? Take a look in $profile_script and choose another option."
	fi
}

function setup_git_commit_user_details {
	echo -e  "\n-------------------------------------------------------"
	echo -en "\n-> Setting up Git. "; git --version

	echo -e "\n-> Your full name, to mark your git commits with (e.g. Joe Bloggs): "
	echo "   (press Enter if default is correct)"
	read -p "   Full Name [$name]: " full_name

	full_name=${full_name:-$name}
	git config --global user.name "$full_name"

	while [[ -z $email_address ]]
	do
		echo -e "\n-> Your email address that GitHub and/or BitBucket knows you by (e.g. example@example.com): "
		email_address_old=$(git config --global --get user.email)
		read -p "   Email Address [$email_address_old]: " email_address
		email_address=${email_address:-$email_address_old}
	done

	git config --global user.email "$email_address"
}

function setup_git_prefs {
	git config --global color.ui true
	git config --global core.autocrlf input
	git config --global push.default simple
	git config --global branch.autosetuprebase always

	git config --global alias.lg "log --oneline --decorate --all --graph"
	git config --global alias.s "status -s"
}

function setup_git_proxy {
	if [[ ! -z $proxy_password ]] && [[ $yoobee_lab == [Yy] ]] ; then
		echo -e "\n-> Permanently saving proxy setup and password to your .gitconfig"

		git config --global http.proxy http://$proxy_username:$proxy_password@$proxy_host
		git config --global https.proxy http://$proxy_username:$proxy_password@$proxy_host
	fi
}

function add_xampp_bin_to_userbin {
	if [[ -z $WINDIR ]] && [[ -d /Applications/XAMPP/bin ]] && [[ $(which php) != "$HOME/bin/php" ]] ; then
		echo -e  "\n-------------------------------------------------------"
		echo -e "\n-> Found XAMPP, symlinking XAMPP's PHP binary into your ~/bin"
		#
		# add_to_bashprofile "export PATH=/Applications/XAMPP/bin:\$PATH # use XAMPP binaries first (esp. php)"
		#   we would do this, but there are lots of binaries in that folder that conflict
		#   with Mac OS X binaries, such as head, which mess up installation of tools such as rvm,
		#   so we make a symlink to the php binary instead:
		#
		ln -s /Applications/XAMPP/bin/php ~/bin/php
		if [[ -z /Applications/XAMPP/xamppfiles/share/openssl/cert.pem ]] ; then
			echo_warning "Your XAMPP folder is missing its certificate authority files."
			echo_warning "If you're an admin, enter your computer password and we'll download them."
			sudo curl -o /Applications/XAMPP/xamppfiles/share/openssl/cert.pem http://curl.haxx.se/ca/cacert.pem
		fi
	fi

	if [[ ! -z $WINDIR ]] && [[ -d /c/xampp/php ]] && [[ ":$PATH:" != *":/c/xampp/bin:"* ]] ; then
		echo -e  "\n-------------------------------------------------------"
		echo -e "\n-> Found XAMPP, adding /c/xampp/php to your PATH"
		#
		# ln -s /c/xampp/php/php.exe ~/bin/php.exe
		#   the symlink technique doesn't work as PHP needs the DLLs in the same folder,
		#   but at least there are no other conflicting binaries in there.
		#
		add_to_bashprofile "export PATH=/c/xampp/php:\$PATH # use XAMPP binaries first (esp. php)"
	fi
}

function add_composer_vendor_bin_to_path {
	if [[ -z $WINDIR ]] && [[ ":$PATH:" != *":$HOME/.composer/vendor/bin:"* ]] ; then
		echo -e "\n-> Adding Composer's ~/.composer/vendor/bin to your PATH"
		add_to_bashprofile "export PATH=\$HOME/.composer/vendor/bin:\$PATH # include Composer global binaries"
	fi
	if [[ ! -z $WINDIR ]] && [[ ":$PATH:" != *":$(cygpath $APPDATA)/Composer/vendor/bin:"* ]]; then
		echo -e "\n-> Adding Composer's $(cygpath $APPDATA)/Composer/vendor/bin to your PATH"
		add_to_bashprofile "export PATH=$(cygpath $APPDATA)/Composer/vendor/bin:\$PATH # include Composer global binaries"
	fi
}

function install_composer {
	if [[ ! -z $(which composer) ]] ; then
		echo -e "\n-------------------------------------------------------"
		echo -e "\n-> PHP Composer already installed."

	else
		echo -e "\n-------------------------------------------------------"
		echo -e "\n-> Installing PHP Composer"

		echo -e "\n-> Downloading and installing Composer\n"
		curl -#Lk https://getcomposer.org/installer | php -- --install-dir=$HOME/bin --filename=composer
		# echo -e "\n-> Renaming composer.phar to composer"
		# mv ~/bin/composer.phar ~/bin/composer
	fi
}

function test_composer {
	echo -e "\n-> Testing Composer..."
	composer diag
	if [[ $? -eq 1 ]] ; then
		echo_error "FATAL: Composer not installed correctly."
		echo " "
		exit
	fi
}

function install_laravel {
	echo -e "\n-> Composer: Installing Laravel globally...\n"
	composer global require laravel/installer
}

function setup_npm_for_user_global {
	if [[ ! -z $(which npm) ]] ; then
		echo -e  "-------------------------------------------------------"
		echo -en "\nNode.JS detected: node "; node -v
		echo -en "\n-> Setting up Node Package Manager (npm). npm v"; npm -v
		if [[ -z $WINDIR ]]; then
			npm config set prefix $HOME
		else
			npm config set prefix $HOME/bin
			# npm on Windows doesn't create/use the bin, etc, lib folders, so we tell it to use ~/bin
		fi
	fi
}

function setup_gulp_globally {
	if [[ ! -z $(which npm) ]] && [[ -z $(which gulp) ]] ; then
		echo -e "\n-> npm: Installing gulp globally...\n"
		npm install --global gulp
		test_for_binary gulp
	fi
}

function goodbye {
	if [[ $bashprofile_uncommented -eq 0 ]] ; then
		add_to_bashprofile " "
		add_to_bashprofile "# End Yoobee Changes"
		add_to_bashprofile " "
	fi
	echo " "
	echo_success " "
	echo_success "WEB DEVELOPMENT CONFIGURATOR COMPLETE!"
	echo_success " "

	echo_info "-> Your file $profile_script has been MODIFIED. Take a look at it."

	if [[ ! -z $proxy_password ]] && [[ $yoobee_lab == [Nn] ]]; then
		echo_warning "Internet access from the command line is not enabled by default."
		echo_warning "When on the Yoobee student network, in order to access the internet, run:"
		echo_warning "    yoobeeproxy"
		echo_warning "in each terminal window you need internet access from."
	fi

	echo_error "*****************************************************************"
	echo_error "*    SETUP SUCCESSFUL, CLOSE ALL YOUR TERMINAL WINDOWS, NOW!    *"
	echo_error "*****************************************************************"

}

function add_to_bashprofile {
	touch $profile_script
	chmod 700 $profile_script

	if [[ $bashprofile_uncommented -eq 1 ]] ; then
		bashprofile_uncommented=0
		echo -e "\n\n#\n# Yoobee Changes $(date)\n#\n" >> $profile_script
	fi
	echo "$1" >> $profile_script
}


# LETS DO THINGS

init_colors

if [[ "$1" == "reset" ]] ; then
	reset_profile
	exit
fi

clear

test_for_binary git

init_variables

# echo_error Test Error Message
# echo_success Test Success Message
# echo_warning Test Warning Message
# echo_info Test Info Message

welcome

detect_proxy

enable_clicolor
add_helpful_aliases

make_sites_directory
make_userbin_directory
add_userbin_to_path

setup_subl_symlink
setup_cli_editor

setup_git_commit_user_details
setup_git_prefs
setup_git_proxy

add_xampp_bin_to_userbin
source $profile_script

test_for_binary php

add_composer_vendor_bin_to_path
source $profile_script

install_composer
test_for_binary composer
test_composer

install_laravel
test_for_binary laravel

setup_npm_for_user_global
setup_gulp_globally

goodbye
